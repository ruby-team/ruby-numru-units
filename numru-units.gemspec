# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'numru/units/version'

Gem::Specification.new do |spec|
  spec.name          = "numru-units"
  spec.version       = Numru::Units::VERSION
  spec.authors       = ["Eizi Toyoda","Takeshi Horinouchi"]
  spec.email         = ["eriko@gfd-dennou.org"]
  spec.summary          = %q{Class to handle units of physical quantities}
  spec.description      = %q{This is a class to handle units of physical quantities. It covers most functionality of UNIDATA's UDUNITS Library, however, with a more sophisticated handling of string expressions. See the documentation for more infomation.}
  spec.homepage         = 'http://www.gfd-dennou.org/arch/ruby/products/numru-units/'
  spec.licenses         = ["Takeshi Horinouchi", "GFD Dennou Club"]

  spec.files         = `git ls-files -z`.split("\x0")
  #spec.extra_rdoc_files = ['./doc/units.rd']
  #spec.extra_rdoc_files = spec.files.grep(%r{^(doc)/})
  #spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  #spec.add_development_dependency "bundler", "~> 1.7"
  #spec.add_development_dependency "rake", "~> 10.0"
end
