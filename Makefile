all:
	@(cd src && make && cp -p units.rb ../lib/numru/)
	@(cd doc && make)

install:
	ruby install.rb

test:
	@(cd src && make test)

distclean: clean

clean:
	@rm -f `find . -name "*~" -print`
