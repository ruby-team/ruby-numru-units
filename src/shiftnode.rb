class ShiftNode < ContainerNode

    include BinaryNode

    def initialize(lhs, rhs)
	@lhs, @rhs = lhs, rhs
    end

    attr_reader :lhs, :rhs
    alias :ref :rhs

    def to_s
	"(#{@lhs.to_s} @ #{@rhs.to_s})"
    end

    def trim2;  @lhs;  end
    def trim
      	self.class.new(@lhs.trim, @rhs.trim2)
    end

    def flatten2;  @lhs;  end
    def flatten
	lf = @lhs.flatten
	case lf
	when ShiftNode
	    rf = lf.rhs.add_eval(@rhs)
	    self.class.new(lf.lhs, rf)
	else
	    self.class.new(lf, @rhs.flatten)
	end
    end

    def sort
	self.class.new(@lhs.sort, @rhs.sort)
    end

    def ref
	case @lhs
	when ShiftNode
	    @lhs.ref.add_eval(@rhs)
	else
	    @rhs
	end
    end

    def deref
	case @lhs
	when ShiftNode
	    @lhs.deref
	else
	    @lhs
	end
    end

    def name
	@lhs.name
    end

    def factor
	@lhs.factor
    end

end
