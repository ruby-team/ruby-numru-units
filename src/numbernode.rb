class NumberNode < TerminalNode

    def initialize(arg)
	raise TypeError unless Numeric === arg
	@a = arg
    end

    UNITY = NumberNode.new(1)
    ZERO = NumberNode.new(0)

    def to_s
      if @a == @a.to_i
        sprintf("%d",@a)
      else
        String(@a)
      end
    end

    attr_reader :a

    alias :value :a
    alias :factor :a

    def == (other)
      case other
      when NumberNode
	@a == other.a
      else
	false
      end
    end

    def add_eval(another)
	raise TypeError unless NumberNode === another
	NumberNode.new(@a + another.value)
    end

    def mul_eval(another)
	case another
	when NumberNode then NumberNode.new(@a * another.a)
	when PowNode
	    raise TypeError unless NumberNode === another.lhs
	    raise TypeError unless NumberNode === another.rhs
	    NumberNode.new(@a * Units::pow_f(another.lhs.value, another.rhs.value))
	else raise TypeError
	end
    end

    def name; "1"; end

    def power;  UNITY;  end

end
