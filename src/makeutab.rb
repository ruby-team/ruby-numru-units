
SI_ABBREV = {
    'E' => 18,		'P' => 15,		'T' => 12,
    'G' => 9,		'M' => 6,		'k' => 3,
    'h' => 2,		'da' => 1,
    'd' => -1,		'c' => -2,
    'm' => -3,		'u' => -6,		'n' => -9,
    'p' => -12,		'f' => -15,		'a' => -18,
}

SI_PREFIX = {
    'exa' => 18,	'peta' => 15,		'tela' => 12,
    'giga' => 9,	'mega' => 6,		'kilo' => 3,
    'hecto' => 2,	'deca' => 1,
    'deci' => -1,	'centi' => -2,
    'milli' => -3,	'micro' => -6,		'nano' => -9,
    'pico' => -12,	'femto' => -15,		'atto' => -18,
}

def plural(string)
    case string
    when /^([^_]+)(_.*)/
	pre, post = $1, $2
	plural(pre) + post
    when /[szoj]$/
	string.sub(/$/, "es")
    when /[^aeou]y$/
	string.sub(/y$/, "ies")
    else
	string.sub(/$/, "s")
    end
end

udefs = {}
ualiases = {}
uplurals = {}

while (line = gets)
    next if /^#/ =~ line
    case line.strip
    when /(\S+)\s+(\S)\s+(\S.*)/
	name, mode, definition = $1, $2, $3
	udefs[name] = definition
    when /(\S+)\s+(\S)/
	name, mode = $1, $2
    else
	next
    end
    case mode
    when /^S/
	for prefix, power in SI_ABBREV
	    next if prefix + name == 'kg'
	    ualiases[prefix + name] = [power, name]
	end
    when /^P/
	pname = plural(name)
	ualiases[pname] = [0, name]
	uplurals[pname] = name
	for prefix, power in SI_PREFIX
	    ualiases[prefix + name] = [power, name]
	end
    end
end

def dumphash(hname, h)
    puts "#{hname} = {"
    s = ""
    for name in h.keys.sort
	a = "  #{name.dump} => #{h[name].inspect},"
	if s.length + a.length > 72
	    puts s
	    s = ""
	end
	s += a
    end
    if s.length > 0
	puts s
	s = ""
    end
    puts "}"
end

puts "class NameNode"
dumphash('UDEFS', udefs)
dumphash('UALIASES', ualiases)
dumphash('UPLURALS', uplurals)
puts "end"
